package br.com.Kabum.bean;

import java.io.Serializable;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.omnifaces.util.Messages;

import br.com.Kabum.dao.FornecedorDAO;
import br.com.Kabum.domain.Fornecedor;

@SuppressWarnings("serial")
@ViewScoped
@ManagedBean(name = "fornecedorBean")
public class FornecedorBean implements Serializable {

	private Fornecedor fornecedor;
	private ArrayList<Fornecedor> fornecedores;

	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	public ArrayList<Fornecedor> getFornecedores() {
		return fornecedores;
	}

	public void setFornecedores(ArrayList<Fornecedor> fornecedores) {
		this.fornecedores = fornecedores;
	}
	
	@PostConstruct
	public void listar() {
		try {
			FornecedorDAO FornecedorDAO = new FornecedorDAO();
			fornecedores = FornecedorDAO.listar();
		} catch (RuntimeException erro) {
			Messages.
			addGlobalError("Ocorreu um erro ao tentar listar os fornecedores");
			erro.printStackTrace();
		}
	}


	public void novo() {
		fornecedor = new Fornecedor();
	}

	public void salvar() {
		try {
			FornecedorDAO FornecedorDAO = new FornecedorDAO();
			FornecedorDAO.salvar(fornecedor);

			fornecedor = new Fornecedor();
			fornecedores = FornecedorDAO.listar();
			Messages.addGlobalInfo("Salvo com sucesso");
		} catch (RuntimeException erro) {
			Messages.addGlobalError("Ocorreu um erro ao tentar salvar o fornecedor");
			erro.printStackTrace();
		}
	}

	public void excluir(ActionEvent evento) {
		try {
			fornecedor = (Fornecedor) evento.getComponent().getAttributes().get("fornecedorSelecionado");
			
			FornecedorDAO FornecedorDAO = new FornecedorDAO();
			FornecedorDAO.excluir(fornecedor);
			
			fornecedores = FornecedorDAO.listar();
			
			Messages.addGlobalInfo("Fornecedor removido com sucesso");
		}catch(RuntimeException erro) {
			Messages.addGlobalError("Ocorreu um erro ao tentar remover o fornecedor!");
			erro.printStackTrace();
		}
		
	}
	
	public void editar(ActionEvent evento) {
		try {
			fornecedor = (Fornecedor) evento.getComponent().getAttributes().get("fornecedorSelecionado");
			FornecedorDAO FornecedorDAO = new FornecedorDAO();
			FornecedorDAO.editar(fornecedor);

			fornecedor = new Fornecedor();
			fornecedores = FornecedorDAO.listar();
			
		} catch (RuntimeException erro) {
			Messages.addGlobalError("Ocorreu um erro ao tentar salvar o fornecedor");
			erro.printStackTrace();
		}
	}

}
