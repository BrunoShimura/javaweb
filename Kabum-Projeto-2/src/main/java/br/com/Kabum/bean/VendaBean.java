package br.com.Kabum.bean;

import java.io.Serializable;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.omnifaces.util.Messages;

import br.com.Kabum.dao.VendaDAO;
import br.com.Kabum.domain.Venda;
import br.com.Kabum.domain.Cliente;

@SuppressWarnings("serial")
@ViewScoped
@ManagedBean(name = "vendaBean")
public class VendaBean implements Serializable {

	private Venda venda;
	private ArrayList<Venda> vendas;
	private ArrayList<Cliente> clientes;

	public Venda getVenda() {
		return venda;
	}

	public void setVenda(Venda venda) {
		this.venda = venda;
	}

	public ArrayList<Venda> getVendas() {
		return vendas;
	}

	public void setVendas(ArrayList<Venda> vendas) {
		this.vendas = vendas;
	}
		public ArrayList<Cliente> getClientes() {
		return clientes;
	}

	public void setClientes(ArrayList<Cliente> clientes) {
		this.clientes = clientes;
	}
	@PostConstruct
	public void listar() {
		try {
			VendaDAO VendaDAO = new VendaDAO();
			vendas = VendaDAO.listar();
		} catch (RuntimeException erro) {
			Messages.
			addGlobalError("Ocorreu um erro ao tentar listar os vendas");
			erro.printStackTrace();
		}
	}


	public void novo() {
		venda = new Venda();
	}

	public void salvar() {
		try {
			VendaDAO VendaDAO = new VendaDAO();
			VendaDAO.salvar(venda);

			venda = new Venda();
			vendas = VendaDAO.listar();
			Messages.addGlobalInfo("Salvo com sucesso");
		} catch (RuntimeException erro) {
			Messages.addGlobalError("Ocorreu um erro ao tentar salvar o venda");
			erro.printStackTrace();
		}
	}

	public void excluir(ActionEvent evento) {
		try {
			venda = (Venda) evento.getComponent().getAttributes().get("VendaSelecionado");
			
			VendaDAO VendaDAO = new VendaDAO();
			VendaDAO.excluir(venda);
			
			vendas = VendaDAO.listar();
			
			Messages.addGlobalInfo("Venda removido com sucesso");
		}catch(RuntimeException erro) {
			Messages.addGlobalError("Ocorreu um erro ao tentar remover o venda!");
			erro.printStackTrace();
		}
		
	}
	
	public void editar(ActionEvent evento) {
		try {
			venda = (Venda) evento.getComponent().getAttributes().get("VendaSelecionado");
			VendaDAO VendaDAO = new VendaDAO();
			VendaDAO.editar(venda);

			venda = new Venda();
			vendas = VendaDAO.listar();
			
		} catch (RuntimeException erro) {
			Messages.addGlobalError("Ocorreu um erro ao tentar salvar o venda");
			erro.printStackTrace();
		}
	}

}
