package br.com.Kabum.bean;

import java.io.Serializable;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.omnifaces.util.Messages;

import br.com.Kabum.dao.Venda_ProdutoDAO;
import br.com.Kabum.domain.Venda_Produto;

@SuppressWarnings("serial")
@ViewScoped
@ManagedBean(name = "venda_produtoBean")
public class Venda_ProdutoBean implements Serializable {

	private Venda_Produto venda_produto;
	private ArrayList<Venda_Produto> vendas_produtos;

	public Venda_Produto getFornecedor() {
		return venda_produto;
	}

	public void setFornecedor(Venda_Produto venda_produto) {
		this.venda_produto = venda_produto;
	}

	public ArrayList<Venda_Produto> getvendas_produtos() {
		return vendas_produtos;
	}

	public void setvendas_produtos(ArrayList<Venda_Produto> vendas_produtos) {
		this.vendas_produtos = vendas_produtos;
	}
	
	@PostConstruct
	public void listar() {
		try {
			Venda_ProdutoDAO Venda_ProdutoDAO = new Venda_ProdutoDAO();
			vendas_produtos = Venda_ProdutoDAO.listar();
		} catch (RuntimeException erro) {
			Messages.
			addGlobalError("Ocorreu um erro ao tentar listar os vendas_produtos");
			erro.printStackTrace();
		}
	}


	public void novo() {
		venda_produto = new Venda_Produto();
	}

	public void salvar() {
		try {
			Venda_ProdutoDAO Venda_ProdutoDAO = new Venda_ProdutoDAO();
			Venda_ProdutoDAO.salvar(venda_produto);

			venda_produto = new Venda_Produto();
			vendas_produtos = Venda_ProdutoDAO.listar();
			Messages.addGlobalInfo("Salvo com sucesso");
		} catch (RuntimeException erro) {
			Messages.addGlobalError("Ocorreu um erro ao tentar salvar o venda_produto");
			erro.printStackTrace();
		}
	}

	public void excluir(ActionEvent evento) {
		try {
			venda_produto = (Venda_Produto) evento.getComponent().getAttributes().get("fornecedorSelecionado");
			
			Venda_ProdutoDAO Venda_ProdutoDAO = new Venda_ProdutoDAO();
			Venda_ProdutoDAO.excluir(venda_produto);
			
			vendas_produtos = Venda_ProdutoDAO.listar();
			
			Messages.addGlobalInfo("Venda_Produto removido com sucesso");
		}catch(RuntimeException erro) {
			Messages.addGlobalError("Ocorreu um erro ao tentar remover o venda_produto!");
			erro.printStackTrace();
		}
		
	}
	
	public void editar(ActionEvent evento) {
		try {
			venda_produto = (Venda_Produto) evento.getComponent().getAttributes().get("fornecedorSelecionado");
			Venda_ProdutoDAO Venda_ProdutoDAO = new Venda_ProdutoDAO();
			Venda_ProdutoDAO.editar(venda_produto);

			venda_produto = new Venda_Produto();
			vendas_produtos = Venda_ProdutoDAO.listar();
			
		} catch (RuntimeException erro) {
			Messages.addGlobalError("Ocorreu um erro ao tentar salvar o venda_produto");
			erro.printStackTrace();
		}
	}

}
