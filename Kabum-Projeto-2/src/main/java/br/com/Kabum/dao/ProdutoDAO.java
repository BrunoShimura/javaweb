package br.com.Kabum.dao;

import java.util.ArrayList;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.Kabum.domain.Produto;
import br.com.Kabum.util.HibernateUtil;


public class ProdutoDAO extends GenericDAO<Produto> {
	
	@SuppressWarnings("unchecked")
	public ArrayList<Produto> buscarPorFornecedor(Long fornecedorCodigo) {
		Session sessao = HibernateUtil.getFabricaDeSessoes().openSession();
		try {
			
			@SuppressWarnings("deprecation")
			Criteria consulta = sessao.createCriteria(Produto.class);
			consulta.add(Restrictions.eq("fornecedor.codigo", fornecedorCodigo));	
			consulta.addOrder(Order.asc("nome"));
			ArrayList<Produto> resultado = (ArrayList<Produto>) consulta.list();
			return resultado;
		} catch (RuntimeException erro) {
			throw erro;
		} finally {
			sessao.close();
		}
	}

}