package br.com.Kabum.dao;

import java.util.ArrayList;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.Kabum.domain.Venda;
import br.com.Kabum.util.HibernateUtil;


public class VendaDAO extends GenericDAO<Venda> {
	
	@SuppressWarnings("unchecked")
	public ArrayList<Venda> buscarPorCliente(Long clienteCodigo) {
		Session sessao = HibernateUtil.getFabricaDeSessoes().openSession();
		try {
			
			@SuppressWarnings("deprecation")
			Criteria consulta = sessao.createCriteria(Venda.class);
			consulta.add(Restrictions.eq("cliente.codigo", clienteCodigo));	
			consulta.addOrder(Order.asc("nome"));
			ArrayList<Venda> resultado = (ArrayList<Venda>) consulta.list();
			return resultado;
		} catch (RuntimeException erro) {
			throw erro;
		} finally {
			sessao.close();
		}
	}

}