package br.com.Kabum.dao;

import java.util.ArrayList;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.Kabum.domain.Venda_Produto;
import br.com.Kabum.util.HibernateUtil;


public class Venda_ProdutoDAO extends GenericDAO<Venda_Produto> {
	
	@SuppressWarnings("unchecked")
	public ArrayList<Venda_Produto> buscarPorVenda(Long vendaCodigo) {
		Session sessao = HibernateUtil.getFabricaDeSessoes().openSession();
		try {
			
			@SuppressWarnings("deprecation")
			Criteria consulta = sessao.createCriteria(Venda_Produto.class);
			consulta.add(Restrictions.eq("venda.codigo", vendaCodigo));	
			consulta.addOrder(Order.asc("nome"));
			ArrayList<Venda_Produto> resultado = (ArrayList<Venda_Produto>) consulta.list();
			return resultado;
		} catch (RuntimeException erro) {
			throw erro;
		} finally {
			sessao.close();
		}
	}
		@SuppressWarnings("unchecked")
	public ArrayList<Venda_Produto> buscarPorProduto(Long produtoCodigo) {
		Session sessao = HibernateUtil.getFabricaDeSessoes().openSession();
		try {
			
			@SuppressWarnings("deprecation")
			Criteria consulta = sessao.createCriteria(Venda_Produto.class);
			consulta.add(Restrictions.eq("produto.codigo", produtoCodigo));	
			consulta.addOrder(Order.asc("nome"));
			ArrayList<Venda_Produto> resultado = (ArrayList<Venda_Produto>) consulta.list();
			return resultado;
		} catch (RuntimeException erro) {
			throw erro;
		} finally {
			sessao.close();
		}
	}
}

