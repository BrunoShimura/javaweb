package br.com.Kabum.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import java.util.Date;

@SuppressWarnings("serial")
@Entity
public class Venda extends GenericDomain {
	@ManyToOne
	@JoinColumn(nullable = false)
	private Cliente cliente;

	@Column(nullable = false)
	@Temporal(TemporalType.DATE)
	private Date data_venda;

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public Date getData_venda() {
		return data_venda;
	}

	public void setData_venda(Date data_venda) {
		this.data_venda = data_venda;
	}
}
