package br.com.Kabum.domain;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@SuppressWarnings("serial")
@Entity
public class Venda_Produto extends GenericDomain {
	@ManyToOne
	@JoinColumn(nullable = false)
	private Venda venda;

	@ManyToOne
	@JoinColumn(nullable = false)
	private Produto produto;

	public Venda getVenda() {
		return venda;
	}

	public void setVenda(Venda venda) {
		this.venda = venda;
	}
	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}
}
